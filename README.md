# projet-chat

## Objectif

Créer une application simplifiée de chat en ligne type Mattermost (slack, discord).

Les fonctionnalités attendues sont les suivantes :
* Créer des salons de discussions persistants
* Echanger en direct sur un salon
* Consulter l'historique des messages des salons
* Voir le status des gens (Optionnel)
* Voir quand quelqu'un est en train d'ecrire (Optionnel)
* D'autres trucs auxquels je pense pas

Vous n'êtes pas obligé de faire un système d'authentification, on peut juste se dire qu'on met un pseudo lorsque l'on
 se connecte.

 ## Mise en Oeuvre

 Pas de contrainte de langage/framework. Il sera à priori préférable d'utiliser les websocket (des librairies existent pour la plupart des langages) pour gérer le temps réel.

 Pour gérer la persistance des messages/salons, il faudra le faire côté serveur avec une bdd, celle que vous voulez. ORM possibles.

 Pour le front, pas de techno imposé, possible de partir sur un framework JS, ou pas.

 Essayer autant que faire se peut de garder une architecture un peu clean, se forcer un peu à pas tout mettre dans le même fichier et à tenter de respecter les concepts du SOLID, aussi bien côté client que côté serveur.
